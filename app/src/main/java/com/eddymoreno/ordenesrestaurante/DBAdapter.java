package com.eddymoreno.ordenesrestaurante;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Eddy Moreno on 11/22/2016.
 */
public class DBAdapter {
    Context c;

    SQLiteDatabase db;
    DBHelper helper;

    public DBAdapter(Context c) {
        this.c = c;
        helper = new DBHelper(c);
    }

    //OPEN DATABASE
    public DBAdapter openDB()
    {
        try {
            db = helper.getWritableDatabase();

        }catch (SQLException e)
        {
            e.printStackTrace();
        }

        return this;
    }

    //CLOSE DATABASE
    public void closeDB()
    {
        try {
          helper.close();

        }catch (SQLException e)
        {
            e.printStackTrace();
        }


    }

    //INSERT
    public long add(String order, String quantity, String price)
    {
        try
        {
            ContentValues cv = new ContentValues();
            cv.put(Constants.ORDER, order);
            cv.put(Constants.QUANTITY, quantity);
            cv.put(Constants.PRICE, price);

            return db.insert(Constants.TB_NAME, Constants.ROW_ID, cv);
        }catch (SQLException e)
        {
            e.printStackTrace();
        }

        return 0;
    }

    //RETRIEVE
    public Cursor getAllOrders()
    {
        String[] columns = {Constants.ROW_ID, Constants.ORDER, Constants.QUANTITY, Constants.PRICE};

        return db.query(Constants.TB_NAME, columns, null, null, null, null, null, null);

    }
}














