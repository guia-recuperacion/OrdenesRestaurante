package com.eddymoreno.ordenesrestaurante;

import android.view.View;

/**
 * Created by Eddy Moreno on 11/22/2016.
 */
public interface ItemClickListener {

    void onItemClick(View v,int pos);
}
