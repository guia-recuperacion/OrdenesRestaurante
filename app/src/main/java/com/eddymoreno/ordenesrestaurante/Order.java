package com.eddymoreno.ordenesrestaurante;
/**
 * Created by Eddy Moreno on 11/22/2016.
 */
public class Order {

    private int id;
    private String order;
    private String quantity;
    private String price;
    private int image;


    public Order(int id, String order, String quantity, String price, int image) {
        this.setId(id);
        this.setOrder(order);
        this.setQuantity(quantity);
        this.setImage(image);
        this.setPrice(price);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
