package com.eddymoreno.ordenesrestaurante;

import android.app.Dialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText nameTxt, posTxt, priceTxt;
    RecyclerView rv;
    MyAdapter adapter;
    ArrayList<Order> orders = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        View navView = LayoutInflater.from(this).inflate(R.layout.activity_main, null); //navigation header menu layout
        TextView StudentNameView = (TextView) navView.findViewById(R.id.tvTotal);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //SHOW INPUT DIALOG
                showDialog();
            }
        });

        //recycler
        rv = (RecyclerView) findViewById(R.id.mRecyclerView);

        //SET PROPS
        rv.setLayoutManager(new LinearLayoutManager(this));

        rv.setItemAnimator(new DefaultItemAnimator());

        //ADAPTER
        adapter = new MyAdapter(this, orders);

        //RETRIEVE
        StudentNameView.setText("C$" + retrieve());

    }

    //SHOW INSERT DIALOG
    private void showDialog()
    {
        Dialog d = new Dialog(this);

        //NO TITLE
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);

        d.setContentView(R.layout.custom_layout);

        nameTxt = (EditText) d.findViewById(R.id.etOrder);
        posTxt = (EditText) d.findViewById(R.id.etQuantity);
        priceTxt = (EditText) d.findViewById(R.id.etPrice);
        Button saveBtn = (Button) d.findViewById(R.id.btnSave);
        final Button retrievebtn = (Button) d.findViewById(R.id.btnRetrieve);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save(nameTxt.getText().toString(), posTxt.getText().toString(), priceTxt.getText().toString());
            }
        });


        retrievebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 retrieve();
            }
        });

        d.show();

    }

    private void save(String order, String quantity, String price)
    {
        DBAdapter db = new DBAdapter(this);

        //OPEN DB
        db.openDB();

        //COMMIT
        long result = db.add(order, quantity, price);

        if(result>0)
        {
            nameTxt.setText("");
            posTxt.setText("");
            priceTxt.setText("");
        }else
        {
            Snackbar.make(nameTxt,"Unable To Save",Snackbar.LENGTH_SHORT).show();
        }

        db.closeDB();

        //REFRESH
        retrieve();
    }

    //RETRIEVE
    float retrieve()
    {
        orders.clear();

        DBAdapter db = new DBAdapter(this);
        db.openDB();

        //RETRIEVE
        Cursor c = db.getAllOrders();

        //LOOP AND ADD TO ARRAYLIST
        float total = 0;
        while (c.moveToNext())
        {
            int id = c.getInt(0);
            String order = c.getString(1);
            String quantity = c.getString(2);
            String price = c.getString(3);
            total = Float.parseFloat(price.toString()) + total;
            Order p = new Order(id, order, quantity, price, R.id.Image);

            //ADD TO ARRAYLIS
            orders.add(p);
        }

        //CHECK IF ARRAYLIST ISNT EMPTY
        if (!(orders.size() < 1))
        {
            rv.setAdapter(adapter);
        }

        db.closeDB();
        return total;
    }


}